<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RouteOneController extends Controller
{
    public function index() {
        return "Selamat datang di route_1! Hanya SuperAdmin yang dapat mengakses halaman ini.";
    }
}
