<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['web', 'auth', 'roles']], function() {
        Route::get('/route-1', [
            'uses' => 'RouteOneController@index',
            'roles' => ['SuperAdmin']
        ])->name('route-1');
        Route::get('/route-2', [
            'uses' => 'RouteTwoController@index',
            'roles' => ['SuperAdmin', 'Admin']
        ])->name('route-2');
        Route::get('/route-3', [
            'uses' => 'RouteThreeController@index',
            'roles' => ['SuperAdmin', 'Admin', 'Guest']
        ])->name('route-3');
});