<?php

spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

class Elang {
    use Hewan, Fight;
    public function __construct($nama) {
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attactPower = 10; 
        $this->defencePower = 5;
    }
    public function getInfoHewan() {
        echo "<b>Biodata Elang </b><br>";
        echo "Nama: " . $this->nama . "<br>";
        echo "Jumlah kaki: " . $this->jumlahKaki . "<br>";
        echo "Keahlian: " . $this->keahlian . "<br>";
        echo "Attact Power: " . $this->attactPower . "<br>";
        echo "Defence Power: " . $this->defencePower . "<br>";
    }
}

class Harimau {
    use Hewan, Fight;
    public function __construct($nama) {
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attactPower = 7; 
        $this->defencePower = 8;
    }
    public function getInfoHewan() {
        echo "<b>Biodata Harimau </b><br>";
        echo "Nama: " . $this->nama . "<br>";
        echo "Jumlah kaki: " . $this->jumlahKaki . "<br>";
        echo "Keahlian: " . $this->keahlian . "<br>";
        echo "Attact Power: " . $this->attactPower . "<br>";
        echo "Defence Power: " . $this->defencePower . "<br><br>";
    }
}

$harimau_1 = new Harimau("harimau_1");
$elang_1 = new Elang("elang_1");

echo "<h1>Battle Raja Hutan</h1>";
echo "<h2>" . $harimau_1->nama . " VERSUS " . $elang_1->nama . "</h2>";

$harimau_1->getInfoHewan();
$elang_1->getInfoHewan();

echo "<h3> !ATRAKSI! </h3>";
echo $harimau_1->atraksi() . "<br>";
echo $elang_1->atraksi();

echo "<h3> !PERTANDINGAN DIMULAI! </h3>";
echo "Percobaan 1: " . $harimau_1->diserang($elang_1) . "! Sisa darah " . $harimau_1->darah . "<br>";
echo "Percobaan 2: " . $harimau_1->serang($elang_1) . "<br>";
echo "Percobaan 3: " . $elang_1->diserang($harimau_1) . "! Sisa darah " . $elang_1->darah . "<br>";
echo "Percobaan 4: " . $elang_1->serang($harimau_1) . "<br>";
echo "Dan seterusnya..."
?>