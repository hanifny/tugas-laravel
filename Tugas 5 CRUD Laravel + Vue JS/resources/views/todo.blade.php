@extends('layouts.master')

@section('content')
<div id="app" class="container">
    <div class="row justify-content-center">
        <div class="col-md-auto col-sm-auto">
            <input v-model="value" @keyup.enter="inputTodo" name="inputTodo" type="text" placeholder="What are you waiting for?" autofocus>
        </div>
    </div>
    <div class="row justify-content-center">
        <div :class="{'completed': todo.isDone}" class="col-md-auto col-sm note" v-for="todo, index in todos">
            <p>@{{todo.name}}</p>
            <button @click="removeTodo(todo, index)" class="remove"><span style="color: #fff;"><i class="fas fa-times-circle"></i></span></button>
            <button @click="toggleComplete(todo)" class="done"><span style="color: #fff;"><i class="fas fa-check-circle"></i></span></button>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    var app = new Vue({
        el: '#app',
        data: {
            value: '',
            todos: []
        },
        methods: {
            inputTodo: function() {
                let input = this.value.trim()
                if (input) {
                    this.$http.post('/api/todo', {name: input}).then(response => {
                        this.todos.unshift({
                            name: input,
                            isDone: 0
                        })
                        this.value = ''
                    })
                }
            },
            removeTodo: function(todo, index) {
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                    if (willDelete) {
                        this.$http.delete('/api/todo/'+todo.id).then(response => {
                            this.todos.splice(index, 1)
                        })
                        swal("Poof! " + todo.name + " has been deleted!", {
                            icon: "success",
                        })
                    } else {
                        swal("Huft! Almost there.");
                    }
                })
            },
            toggleComplete: function(todo) {
                if (todo.isDone == 0) {
                    swal("Good job!", "You've done'!", "success");
                }
                this.$http.patch('/api/todo/'+todo.id).then(response => {
                    todo.isDone = !todo.isDone
                })
            }
        },
        mounted: function() {
            this.$http.get('/api/todo').then(response => {
                // success callback
                this.todos = response.body.data
                console.log(response.body.data);
                
            }, response => {
                // error callback
            });
        }
    });
</script>
@endpush