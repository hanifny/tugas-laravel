<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;
use App\User;
use App\Http\Resources\TodoResource;

class TodoController extends Controller
{
    public function index() {   
        $todos = Todo::orderBy('created_at', 'desc')->get();
        return TodoResource::collection($todos);
    }

    public function store(Request $request) {
        $todo = Todo::create([
            'name'    => $request->name,
            'isDone'  => 0
        ]);

        return new TodoResource($todo);
    }

    public function changeDoneStatus($id) {
        $todo = Todo::find($id);
        if($todo->isDone == 1) {
            $update = 0;
        } else {
            $update = 1;
        }
        $todo->update([
            'isDone' => $update
        ]);
        return new TodoResource($todo);
    }

    public function destroy($id) {
        Todo::destroy($id);
        return 'success';
    }
}
