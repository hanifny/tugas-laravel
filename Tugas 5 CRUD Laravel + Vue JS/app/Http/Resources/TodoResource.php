<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TodoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'isDone'     => $this->isDone,
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }

    public function with($request) {
        return ['status' => 'success'];
    }
}