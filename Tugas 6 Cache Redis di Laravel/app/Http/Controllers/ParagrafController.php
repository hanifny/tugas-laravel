<?php

namespace App\Http\Controllers;

use App\Paragraf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ParagrafController extends Controller
{
    public function cache() {
        $query = Cache::remember('paragraf', 60, function() {
            return Paragraf::all();
        });
        return view('paragraf', compact('query'));
    }

    public function query() {
        $query = Paragraf::all();
        return view('paragraf', compact('query'));
    }
}
